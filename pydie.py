from tkinter import *
from functools import partial
import random

class DiceWindow(Frame):
    def __init__(self, parent):
        self.parent = parent
        #self.parent.attributes("-fullscreen",True)
        self.parent.title("Dice Roller by Ethan Carter")
        self.parent.option_add("*Button.Background", "black")
        self.parent.option_add("*Label.Background", "black")
        self.parent.option_add("*Button.Foreground", "cyan")
        self.parent.option_add("*Label.Foreground", "orange")
        self.parent.configure(background="black")
        self.parent.configure(borderwidth="10")
        self.parent.option_add("*Font","impact 24")
        self.parent.geometry('480x320')
        self.parent.grid_propagate(False)
        self.dice, self.sides, self.result = 0, 0, 0
        self.tally = []
        
        self.str_dice = StringVar(self.parent)
        self.str_dice.set(str(self.dice))

        self.str_sides = StringVar(self.parent)
        self.str_sides.set(str(self.sides))

        self.str_result = StringVar(self.parent)
        self.str_result.set(str(self.result))

        self.str_tally = StringVar(self.parent)
        self.str_tally.set(str(self.tally))

        self.add_dice1 = partial(self.add_dice, x=100, y=1)
        self.add_dice10 = partial(self.add_dice, x=91, y=10)
        self.minus_dice1 = partial(self.minus_dice, x=0, y=1)
        self.minus_dice10 = partial(self.minus_dice, x=9, y=10)
        self.add_sides1 = partial(self.add_sides, x=100, y=1)
        self.add_sides10 = partial(self.add_sides, x=91, y=10)
        self.minus_sides1 = partial(self.minus_sides, x=0, y=1)
        self.minus_sides10 = partial(self.minus_sides, x=9, y=10)
        
        self.plusone = Button(self.parent, text="+1")
        self.plusone.bind("<Button-1>", self.add_dice1)

        self.plusten = Button(self.parent, text="+10")
        self.plusten.bind("<Button-1>", self.add_dice10)

        self.dicelabel = Label(self.parent, textvariable=self.str_dice)

        self.minusone = Button(self.parent, text="-1")
        self.minusone.bind("<Button-1>", self.minus_dice1)

        self.minusten = Button(self.parent, text="-10")
        self.minusten.bind("<Button-1>", self.minus_dice10)

        self.plusten.grid(row=0, sticky=N+S+E+W)
        self.plusone.grid(row=1, sticky=N+S+E+W)
        self.dicelabel.grid(row=2)
        self.minusone.grid(row=3, sticky=N+S+E+W)
        self.minusten.grid(row=4, sticky=N+S+E+W)

        self.d_label = Label(self.parent, text= "d")
        self.d_label.grid(rowspan=5, row=0,column=1, sticky=NSEW)

        self.side_plusone = Button(self.parent, text="+1")
        self.side_plusone.bind("<Button-1>", self.add_sides1)

        self.side_plusten = Button(self.parent, text="+10")
        self.side_plusten.bind("<Button-1>", self.add_sides10)
        self.sidelabel = Label(self.parent, textvariable=self.str_sides)
        self.side_minusone = Button(self.parent, text="-1")
        self.side_minusone.bind("<Button-1>", self.minus_sides1)

        self.side_minusten = Button(self.parent, text="-10")
        self.side_minusten.bind("<Button-1>", self.minus_sides10)
        self.roll_button = Button(self.parent, text="ROLL")
        self.roll_button.bind("<Button-1>", self.roll_dice)

        self.result_label = Label(self.parent, textvariable=self.str_result)
        self.tally_label = Label(self.parent, textvariable=self.str_tally, font=("Impact 10"), wraplength=150, anchor=NW, justify=LEFT, width=25)
        self.side_plusten.grid(row=0, column=2, sticky=N+S+E+W)
        self.side_plusone.grid(row=1, column=2, sticky=N+S+E+W)
        self.sidelabel.grid(row=2,column=2)
        self.side_minusone.grid(row=3, column=2, sticky=N+S+E+W)
        self.side_minusten.grid(row=4, column=2, sticky=N+S+E+W)
        self.roll_button.grid(row=3,rowspan=2, column=3,sticky=N+W+E+S)
        self.result_label.grid(row=2, column=3, sticky=N+W+E+S)
        self.tally_label.grid(row=0, rowspan=2, column=3,sticky=NS)

        for x in range(5):
            Grid.rowconfigure(self.parent, x, weight =1)
        for y in range(4):
            Grid.columnconfigure(self.parent, y, weight =1)

    def add_dice(self, event, x, y):
        if self.dice < x:
            self.dice += y
        self.str_dice.set(str(self.dice))

    def minus_dice(self, event, x, y):
        if self.dice > x:
            self.dice -= y
        self.str_dice.set(str(self.dice))
        
    def add_sides(self, event, x, y):
        if self.sides < x:
            self.sides += y
        self.str_sides.set(str(self.sides))

    def minus_sides(self, event, x, y):
        if self.sides > x:
            self.sides -= y
        self.str_sides.set(str(self.sides))

    def roll_dice(self, event):
        result_list = []
        for x in range(self.dice):
            result_list.append(random.randint(1, self.sides))

        result = sum(result_list)
        self.str_result.set(str(result))
        self.str_tally.set('  '.join(str(x) for x in result_list))
        
        print("Individual Dice:")                       
        print('  '.join(str(x) for x in result_list))
        print("Total:")
        print(str(result))

if __name__ == "__main__":
    root = Tk()
    window = DiceWindow(root)
    root.mainloop()
